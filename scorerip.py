#!/usr/bin/env python3

#  scorerip
#  Copyright (C) 2019 contact@t3Y.eu
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import os
import shutil
import argparse
import cv2
import numpy
from weasyprint import HTML, CSS

verbose = False
path = os.path.dirname(__file__)

threshold = 100
def main():
    limit = 60
    step = 1500
    keep = False
    global threshold
    margin = 0

    parser = argparse.ArgumentParser()
    parser.add_argument("input_file", help="The video file that should be used as input")
    parser.add_argument("-v", "--verbose", help="Display additional information while the program is running", action="store_true")
    parser.add_argument("-k", "--keep", help="Keep temporary files, can be used to manually make the score from the temp files", action="store_true")
    parser.add_argument("-s", "--step", help="Change the interval between frames considered for reading and comparing, given in milliseconds. Higher values will reducde processing time but may lead to parts of the score being skipped. Default value is {}".format(step))
    parser.add_argument("-l", "--limit", help="Change the maximal number of frames to extract from a video in case something goes wrong, default is {}".format(limit))
    parser.add_argument("-t", "--threshold", help="Change the color intensity (0-765) below which a pixel should be considered to be a foreground element, default is {}".format(threshold))
    parser.add_argument("-m", "--margin", help="Change the pdf document margin, default is {}".format(margin))

    args = parser.parse_args()

    input_file = args.input_file

    if args.verbose:
        global verbose
        verbose = True

    if args.keep:
        keep = True

    if args.step:
        try:
            step = int(args.step)
        except:
            out("WARNING: invalid value for argument --step, using default")

    if args.limit:
        try:
            limit = int(args.limit)
        except:
            out("WARNING: invalid value for argument --limit, using default")

    if args.threshold:
        try:
            temp = int(args.threshold)
            if temp > 0 and temp <= 765:
                threshold = int(args.intensity)
            else:
                out("WARNING: invalid value for argument --intensity, using default")
        except:
            out("WARNING: invalid value for argument --intensity, using default")

    out("using OpenCV v{}".format(cv2.__version__))
    out("frame limit: {} frames".format(limit))
    out("step: {}ms".format(step))
    out("threshold: {}".format(threshold))

    try:
        os.mkdir("temp")
    except:
        pass

    if not os.path.exists(input_file):
        out("ERROR: file {} could not be found".format(input_file))
        return

    count = extractFrames(input_file, "temp", limit, step)
    htmlFile = generateHTML(input_file.split(".")[0], count, "temp")
    generatePDF(htmlFile, input_file.split(".")[0], margin)
    if not keep:
        cleanup(htmlFile)
    out(" ")
    out("all done!")

def cleanup(htmlFile):
    out(" ")
    out("cleaning up")
    out("removing extracted frames")
    shutil.rmtree("temp")
    out("removing html file")
    os.remove(htmlFile)
    out("done.")


def generatePDF(i, o, margin):
    out(" ")
    out("generating PDF file")

    css = CSS(string="@page {{margin: {}cm}}".format(margin))
    HTML(i).write_pdf("{}.pdf".format(o), stylesheets=[css])
    
def generateHTML(filename, images, tempfolder):
    out(" ")
    out("generating html file")
    file = open("{}.html".format(filename), "w")
    file.write 
    file.write("<center>\n")
    for i in range(images):
        file.write('<img src="{}" style="width:100%">\n'.format(os.path.join(tempfolder, "frame{}.jpg".format(i))))

    file.write("</center>")
    file.close()
    return ("{}.html".format(filename))
    out("done.")

def extractFrames(i, o, limit, step):
    out(" ")
    out("extracting frames")
    out(" ")
    readFrames = 0
    extractedFrames = 0
    currentframe = []
    vid = cv2.VideoCapture(i)

    success,previousFrame = vid.read()
    previousFrame = crop(previousFrame)
    scheme = makeScheme(previousFrame)
    if len(scheme) > 0:
        out("first frame is not blank, saving...")
        cv2.imwrite(os.path.join(o, "frame{}.jpg".format(extractedFrames)), previousFrame)
        extractedFrames += 1
    readFrames += 1


    
    while success and extractedFrames < limit:
        out("reading frame at {}".format(formatTime(readFrames * step)))
        vid.set(cv2.CAP_PROP_POS_MSEC,(readFrames*step))
        success,currentFrame = vid.read()
        if success: 
            currentFrame = crop(currentFrame)
            readFrames += 1
            scheme,diff = different(currentFrame, scheme)
        if diff:
            out("current frame appears to be new, saving...")
            cv2.imwrite(os.path.join(o, "frame{}.jpg".format(extractedFrames)), currentFrame)
            previousFrame = currentFrame
            extractedFrames += 1
    out(" ")
    out("done, extracted {} frames".format(extractedFrames))
    return extractedFrames

#compares an image to previously computed array of coordinates of dark pixels
def different(newImage, previousScheme):
    global threshold
    differences = 0
    #for each pixel that was dark in the previous saved frame, check if it's still dark in the current frame
    for i in range(len(previousScheme)):
        #if difference is already too high, just stop
        if differences > len(previousScheme)*0.1:
            break;
        x = previousScheme[i][0]
        y = previousScheme[i][1]
        intensity = 0
        try:
            for j in range(3):
                intensity += newImage[x][y][j]
            if intensity > threshold:
                differences += 1
        except IndexError as e:
            #image was cropped differently, it's a different image
            differences = len(previousScheme)
            break;
    #if over 10% of pixels don't match, the frame is probably new
    if differences > len(previousScheme)*0.1:
        out("difference to previous: {}".format(differences))
        s = makeScheme(newImage)
        #if the new image is blank, don't save it
        if len(s) == 0:
            return s,False
        return s,True
    #if the previous image was blank, save the new image if it isn't blank
    elif len(previousScheme) == 0:
        s = makeScheme(newImage)
        out("difference to previous: {}".format(len(s)))
        if len(s) == 0:
            return s,False
        return s,True
    #otherwise, the new image is probably the same as previously
    else:
        out("difference to previous: {}".format(differences))
        return previousScheme,False

#returns an array of coordinates of pixels that are darker than R + G + B = threshold
def makeScheme(image):
    rows = len(image)
    cols = len(image[0])
    global threshold
    scheme = []
    for i in range(rows):
        for j in range(cols):
            intensity = 0
            for k in range(3):
                intensity += int(image[i][j][k])
                if intensity > threshold:
                    break
            if intensity <= threshold:
                scheme.append([i,j])
    return scheme

def crop(image):
    global threshold
    h,w,d = image.shape
    #left limit
    for i in range(w):
        if numpy.max(image[:,i,:]) > threshold*2:
            break
    #right limit
    for j in range(w-1,0,-1):
        if numpy.max(image[:,j,:]) > threshold*2:
            break
    #top limit
    for k in range(h):
        if numpy.max(image[k,:,:]) > threshold*2:
            break
    #bottom limit
    for l in range(h-1,0,-1):
        if numpy.max(image[l,:,:]) > threshold*2:
            break

    cropped = image[k:l,i:j+1,:].copy() # deep copy to get byte-aligned array needed for opencv 
    if len(cropped) > 0:
        return cropped
    else:
        out("WARNING: could not crop frame, threshold might be too high")


# print text only when --verbose is on
def out(arg):
    global verbose
    if verbose == True:
        if arg != " ":
            print("[scorerip] {}".format(arg))
        else:
            print(" ")

def formatTime(ms):
    h = padWith0(ms // 3600000)
    m = padWith0((ms // 60000) % 60)
    s = padWith0((ms // 1000) % 60)
    ms = ms % 1000
    return "{}:{}:{}.{}".format(h,m,s,ms)

def padWith0(nb):
    if nb < 10:
        return "0{}".format(nb)
    else:
        return nb
main()

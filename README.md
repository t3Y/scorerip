# scorerip

Rip sheet music off sheet music videos. Uses OpenCV and weasyprint.

## Reminder

Please remember to respect your country's copyright laws.

## Installation

You will need `python3` and `pip3`

Clone the directory
```
git clone git@gitlab.com:t3Y/scorerip.git
```

Install dependencies
(you should probably use a virtual environment, but I have no clue how they work)

```
cd scorerip
pip3 install -r requirements.txt
```

## Usage

Download a sheet music video from your favourite streaming platform using your prefered tool and pass the file as argument to scorerip. It should output a neat pdf containing the score. 

Use `python3 scorerip -h` for additional usage information:

usage: `scorerip.py [-h] [-v] [-k] [-s STEP] [-l LIMIT] [-t THRESHOLD] [-m MARGIN] input_file`

In its current state scorerip is extremly slow. It is recommended to use the `--verbose` argument so that at least you can have an assurance that the programm is making progress and not just eating processing ressources. It looks kinda nice too.
